package com.epam.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SortingApp {
    List<Integer> list = new ArrayList<>();

    public void sort(int... a) {
        if (a == null) {
            throw new IllegalArgumentException();
        }
        if (a.length > 10) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < a.length - 1; i++) {
            for (int j = 0; j < a.length - 1; j++) {
                if (a[j] > a[j + 1]) {
                    int t = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = t;
                }
            }
        }
        for (int i : a) {
            list.add(i);
        }
    }

    public String listToString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < list.size() - 1; i++) {
            result.append(list.get(i)).append(" ");
        }
        result.append(list.get((list.size() - 1)));
        return result.toString();
    }

    public static void main(String[] args) {
        int amount = 11;
        SortingApp sorting = new SortingApp();
        int a1;
        int a2;
        int a3;
        int a4;
        int a5;
        int a6;
        int a7;
        int a8;
        int a9;
        int a10;
        System.out.println("How many variables to sort: ");
        Scanner scanner = new Scanner(System.in);
        amount = scanner.nextInt();
        while (amount > 10) {
            System.out.println("Insert 10, or less!");
            amount = scanner.nextInt();
        }
        if (amount == 1) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            sorting.sort(a1);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 2) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            sorting.sort(a1, a2);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 3) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            sorting.sort(a1, a2, a3);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 4) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 5) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            a5 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4, a5);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 6) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            a5 = scanner.nextInt();
            a6 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4, a5, a6);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
            ;
        }
        if (amount == 7) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            a5 = scanner.nextInt();
            a6 = scanner.nextInt();
            a7 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4, a5, a6, a7);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 8) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            a5 = scanner.nextInt();
            a6 = scanner.nextInt();
            a7 = scanner.nextInt();
            a8 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4, a5, a6, a7, a8);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 9) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            a5 = scanner.nextInt();
            a6 = scanner.nextInt();
            a7 = scanner.nextInt();
            a8 = scanner.nextInt();
            a9 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4, a5, a6, a7, a8, a9);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
        if (amount == 10) {
            System.out.println("Insert values: ");
            a1 = scanner.nextInt();
            a2 = scanner.nextInt();
            a3 = scanner.nextInt();
            a4 = scanner.nextInt();
            a5 = scanner.nextInt();
            a6 = scanner.nextInt();
            a7 = scanner.nextInt();
            a8 = scanner.nextInt();
            a9 = scanner.nextInt();
            a10 = scanner.nextInt();
            sorting.sort(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10);
            System.out.println("After sort: ");
            System.out.println(sorting.listToString());
        }
    }
}
