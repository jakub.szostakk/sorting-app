package com.epam.demo;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class SortingAppZeroArgumentsTest {
    protected SortingApp sorting = new SortingApp();

    @Test(expected = IndexOutOfBoundsException.class)
    public void zeroArgumentsTest() {
        sorting.sort();
        sorting.listToString();
    }
}