package com.epam.demo;

import org.junit.*;

public class SortingAppNullCaseTest {
    protected SortingApp sorting = new SortingApp();

    @Test(expected = IllegalArgumentException.class)
    public void nullCase() {
        sorting.sort(null);
    }
}
