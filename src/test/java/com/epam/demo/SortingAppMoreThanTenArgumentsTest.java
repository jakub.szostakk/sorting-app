package com.epam.demo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppMoreThanTenArgumentsTest {
    protected SortingApp sorting = new SortingApp();

    private int a1;
    private int a2;
    private int a3;
    private int a4;
    private int a5;
    private int a6;
    private int a7;
    private int a8;
    private int a9;
    private int a10;
    private int a11;

    private String expected;

    public SortingAppMoreThanTenArgumentsTest(int a1, int a2, int a3,
                                              int a4, int a5, int a6,
                                              int a7, int a8, int a9,
                                              int a10, int a11) {
        this.a1 = a1;
        this.a2 = a2;
        this.a3 = a3;
        this.a4 = a4;
        this.a5 = a5;
        this.a6 = a6;
        this.a7 = a7;
        this.a8 = a8;
        this.a9 = a9;
        this.a10 = a10;
        this.a11 = a11;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {10, 1, 9, 2, 8, 3, 7, 4, 6, 5, 12},
                {1, 56, 375, 25, 7245, 24, 62, 245, 264, 2, 31},
                {36, 4, 1364, 13, 64, 21, 8, 42, 67, 3, 63},
                {35, 346, 1346, 12, 62, 13, 7, 2, 63, 1, 0},
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArguments() {
        sorting.sort(a1, a2, a3, a4, a5, a6, a7, a8, a9, a1, a11);
    }
}
