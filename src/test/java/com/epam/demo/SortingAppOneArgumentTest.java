package com.epam.demo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class SortingAppOneArgumentTest {
    protected SortingApp sorting = new SortingApp();

    private int a;
    private String expected;

    public SortingAppOneArgumentTest(int a, String expected) {
        this.a = a;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, "1"},
                {14, "14"},
                {47, "47"},
                {1000, "1000"},
        });
    }

    @Test
    public void testOneArgument() {
        sorting.sort(a);
        String result = sorting.listToString();
        assertEquals(expected, result);
    }
}
